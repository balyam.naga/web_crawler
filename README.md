Hello everyone welcome to the web_crawler project

About the project:-
1) This is a web scraper where we scrap through the internet based on the base url provided
customised such that n number of pages can be scraped and all the scraped data is stored in a database.

2) We are storing all the links so that we can find the shortest path between two links and the distance between them. 

3)How to create virtual environment
virtualenv <enivironment name>
cd <environment name>
cd <bin>
source ./activate to activate the virtualenv

4)
Project structure:-
Spider.py which consists Spider class
Sqll.py which manages all the sql related queries
digrap.py which consists of functions to find the shortest path and lengths between them.

Sql table structure.
CREATE TABLE URL_DATA_CODES(
                                            id varchar PRIMARY KEY,
                                            content varchar NOT NULL,
                                            status_code integer 

                                        );
CREATE TABLE URL_DATA_LINKS(
                                            source varchar NOT NULL,
                                            destination varchar NOT NULL                                           
                                        );

5)pip install -r requirements.txt

HAVE FUN SCRAPING THE INTERNET. :)

