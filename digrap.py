import networkx as nx

import sqll


class Digraph():

    def __init__(self):
        self.db = sqll.database("crawldb.db")
        self.graph = self.creategraph()

    def getCursor(self):
        return self.db.fetch_data_forgraph()

    def creategraph(self):
        g = nx.DiGraph()
        data = self.getCursor()
        for item in data:
            g.add_edge(item[0], item[1], weight=1)   
        return g

    def findPath(self, source, destination):
        return nx.dijkstra_path(self.graph, source, destination)

    def findshortestlength(self, source, destination):
        return nx.shortest_path_length(self.graph, source, destination)


if __name__ == "__main__":
    graph = Digraph()
    print(graph.findPath("http://0.0.0.0:8000/", "http://0.0.0.0:8000/book92.html"))
    print(graph.findshortestlength("http://0.0.0.0:8000/", "http://0.0.0.0:8000/book92.html"))
