import requests
import sqll
from urllib.parse import urlparse
from bs4 import BeautifulSoup


class spider():
    
    def __init__(self, baseurl, waiting):
        self.db = sqll.database('crawldb.db')
        self.url = baseurl
        self.waiting = waiting
                  
    def crawl(self, url):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                return (response.status_code, response.text)
        except:
            return (response.status_code, "error")

    def crawl_links(self, url):
        if self.db.takecount() > 100:
            return
        print("connecting to-->", url)
        status_code, content = self.crawl(url)
        print(status_code)

        if status_code == 200:
            soup = BeautifulSoup(content, "html.parser")
            links = soup.findAll("a")
            islinkcrawled = self.db.checkUrl(url)
#           Checking whether link is crawled or not
            if islinkcrawled != True:
               
                self.db.insert_into_table_content(url, content, status_code)
                for ref in links:
                    link = urlparse(ref.get('href'))
#                    if path not empty and netloc is not present then we have to append to baseurl 
                    if ref.get('href') is not None and link.path is not None and link.path != '' and ref.get('href') !='':
                        if link.netloc == '':
                            if self.db.checkUrl(self.url+link.path) != True:
                                self.waiting.append(self.url+link.path)
                                self.db.insert_into_table_links(url, self.url + link.path)
#                    path may be empty and netloc is present             
                    elif link.netloc == urlparse(self.url).netloc:
                        if self.db.checkUrl(ref.get('href')) != True:  
                            self.waiting.append(ref.get('href'))
                            self.db.insert_into_table_links(url, ref.get('href'))
#               Recursively calling every link present in the waiting list    
                for val in self.waiting:
                    self.waiting.remove(val)
                    if self.db.checkUrl(val) != True:
                        self.crawl_links(val) 
            else:
                return
        else:
            if self.db.checkUrl(url) != True:
                sqll.insert_into_table_content(url, 'error', 404)
                print('or here?')
                return


if __name__ == "__main__":                
    s = spider('http://0.0.0.0:8000/', [])
    s.crawl_links('http://0.0.0.0:8000/')
    print('=======================================================')






