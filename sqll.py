import sqlite3

from sqlite3 import Error


class database():

    def __init__(self, db_file):
        self.conn = self.create_connection(db_file)
        self.create_table_content()
        self.create_table_links()

    def create_connection(self, db_file):
        try:
            connection = sqlite3.connect(db_file)
            return connection
        except Error as e:
            print(e)

    def create_table_content(self):
        try:
            c = self.conn.cursor()
            sql_table_content = """  
                   CREATE TABLE IF NOT EXISTS URL_DATA_CODES(
                                            id varchar PRIMARY KEY,
                                            content varchar NOT NULL,
                                            status_code integer 

                                        ); """
            c.execute(sql_table_content)
            print("succesful")
        except Error as e:
            print(e)

    def create_table_links(self):
        try:
            c = self.conn.cursor()
            sql_table_links = """
                   CREATE TABLE IF NOT EXISTS URL_DATA_LINKS(
                                            source varchar NOT NULL,
                                            destination varchar NOT NULL                                           
                                        ); """
            c.execute(sql_table_links)
            print("succesful")
        except Error as e:
            print(e)

    def insert_into_table_content(self, url, content, status_code):
        try:
            c = self.conn.cursor()
            c.execute("insert into URL_DATA_CODES values(?,?,?)", (url, content, status_code))
            self.conn.commit()
        except Error as e:
            print(e)

    def insert_into_table_links(self, source, destination):
        try:
            c = self.conn.cursor()
            c.execute("insert into URL_DATA_LINKS values(?,?)", (source, destination))
            self.conn.commit()
        except Error as e:
            print(e)

    def takecount(self):
        try:         
            cur = self.fetch_data("select count(id) from URL_DATA_CODES;")
            for item in cur:
                return item[0]
        except:
            return 0

    def fetch_data(self, query):
        try:
            c = self.conn.cursor()
            c.execute(query)
            self.conn.commit()
            #print("succesfully fetched id")
            return c
        except Error as e:
            print(e)        

    def checkUrl(self, url):
        urlcheck = False
        c = self.conn.cursor()
        c.execute("select URL_DATA_CODES.id from URL_DATA_CODES where id = ?", (url,))
        self.conn.commit()
        for item in c:
            if item[0] == url:
                urlcheck = True
                return urlcheck
                         
    def fetch_data_forgraph(self):
        try:
            c = self.conn.cursor()
            query = ("""
            select source,destination from URL_DATA_LINKS;
            """)
            c.execute(query)
            self.conn.commit()
            print("succesful")
            return c
        except Error as e:
            print(e)
