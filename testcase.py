import unittest
from spider import spider as n
from sqll import database as db
from digrap import Digraph as grp


class Testcrawler(unittest.TestCase):
   
    def setUp(self):
        database = db('crawldb.db')
        con = database.create_connection('crawldb.db')
        c = con.cursor()
        sql_table_content = """  
                   CREATE TABLE IF NOT EXISTS URL_DATA_CODES(
                                            id varchar PRIMARY KEY,
                                            content varchar NOT NULL,
                                            status_code integer 

                                        ); """
        c.execute(sql_table_content)
        sql_table_links = """
                   CREATE TABLE IF NOT EXISTS URL_DATA_LINKS(
                                            source varchar NOT NULL,
                                            destination varchar NOT NULL                                           
                                        ); """
        c.execute(sql_table_links)

    def tearDown(self):
        database = db('crawldb.db')
        con = database.create_connection('crawldb.db')
        c = con.cursor()
        c.execute("DROP TABLE URL_DATA_CODES;")
        c.execute("DROP TABLE URL_DATA_LINKS")
        print("data cleaned")    

    def test_crawl(self):
        url = "http://0.0.0.0:8000/"
        statuscode, res = n('http://0.0.0.0:8000/', []).crawl(url)
        expectedcode = 200
        actualcode = statuscode
        self.assertEquals(expectedcode, actualcode)

    def test_crawl_links(self):
        url = "http://0.0.0.0:8000/"
        temp = n('http://0.0.0.0:8000/', [])
        temp.crawl_links(url)
        database = db('crawldb.db')
        con = database.create_connection('crawldb.db')
        c = con.cursor()
        c.execute("select id from URL_DATA_CODES where id = ?", ("http://0.0.0.0:8000/book92.html",))
        con.commit()
        expected_output = True
        if c is None:
            actual_output = False
        else:
            actual_output = True
        self.assertEquals(expected_output, actual_output)

    def test_short_path_length(self):
        self.test_crawl_links()
        temp = grp()         
        actual_output = temp.findshortestlength("http://0.0.0.0:8000/","http://0.0.0.0:8000/book92.html")   
        expected_output = 2
        self.assertEquals(actual_output, expected_output)

    def test_short_path(self):
        self.test_crawl_links()
        temp = grp()
        actual_output = temp.findPath("http://0.0.0.0:8000/", "http://0.0.0.0:8000/book92.html")
        expected_output = ['http://0.0.0.0:8000/', 'http://0.0.0.0:8000/book9.html', 'http://0.0.0.0:8000/book92.html']
        self.assertEquals(actual_output, expected_output)    


if __name__ == '__main__': 
    unittest.main() 




